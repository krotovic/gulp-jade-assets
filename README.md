# gulp-jade-assets
This is gulp plugin for including various files dynamically in some jade file.

## Installation

Use NPM for installing as development requirement like so:
```
$ npm i -D gulp-jade-assets
```

## Usage

In your `gulpfile.js` create new task:
```js

var gulp = require('gulp');
var jadeAssets = require('gulp-jade-assets');

gulp.task('jade', function () {
    gulp.src('templates/**.jade')
        .pipe(jadeAssets('scripts/**.js', 'templates/scripts.jade'))
        .pipe(gulp.dest('app'));
});
```

It will render all files in `scripts/**.js` folder and subfolders as scripts (or by file extension) to specified jade template.

## API

### gulp-jade-assets(_filesToInclude_, _outputFile_)

- **filesToInclude** _String_ path as string to files to include in
- **outputFile** _String (optional)_ if specified it is path to output file (either Jade template or HTML file), 
otherwise it uses index.(jade|html) of files specified in `gulp.src(...)` or creates new one.
